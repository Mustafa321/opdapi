using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using OPDAPI.Controllers;
using OPDAPI.Models;

namespace MyChatHub
{
 //   public class ConnectionMapping<T>
 //   {
 //       private readonly Dictionary<T, HashSet<string>> _connections =
 //       new Dictionary<T, HashSet<string>>();
 //       private int Count
 //       {
 //           get
 //           {
 //               return _connections.Count;
 //           }
 //       }
 //       public void Add(T key, string connectionId)
 //       {
 //           lock (_connections)
 //           {
 //               HashSet<string> connections;
 //               if (!_connections.TryGetValue(key, out connections))
 //               {

 //                   connections = new HashSet<string>();
 //                   _connections.Add(key, connections);
 //               }
 //               lock (connections)
 //               {
 //                   connections.Add(connectionId);
 //               }
 //           }
 //       }
 //       public IEnumerable<string> GetConnections(T key)
 //       {
 //           HashSet<string> connections;
 //           if (_connections.TryGetValue(key, out connections))
 //           {
 //               return connections;
 //           }
 //           return Enumerable.Empty<string>();
 //       }
 //       public void Remove(T key, string connectionId)
 //       {
 //           lock (_connections)
 //           {
 //               HashSet<string> connections;
 //               if (!_connections.TryGetValue(key, out connections))
 //               {
 //                   return;
 //               }
 //               lock (connections)
 //               {
 //                   connections.Remove(connectionId);
 //                   if (connections.Count == 0)
 //                   {
 //                       _connections.Remove(key);
 //                   }
 //               }
 //           }
 //       }

 //   }
 //   public class ChatHub : Hub
	//{
	//	private static readonly ConnectionMapping<string> _connections = new ConnectionMapping<string>();

	//	private static List<MessageDetail> CurrentMessage = new List<MessageDetail>();

	//	public ChatController obj = new ChatController();

	//	public override Task OnConnected()
	//	{
	//		string text = base.Context.QueryString["username"];
	//		_connections.Add(text, base.Context.ConnectionId);
	//		obj.UserConnected(text, base.Context.ConnectionId);
	//		base.Clients.All.userConnected(text);
	//		return base.OnConnected();
	//	}

	//	public override Task OnDisconnected(bool stopCalled)
	//	{
	//		string text = base.Context.QueryString["username"];
	//		_connections.Remove(text, base.Context.ConnectionId);
	//		obj.UserDisconnected(base.Context.ConnectionId);
	//		base.Clients.All.userDisConnected(text);
	//		return base.OnDisconnected(stopCalled);
	//	}

	//	public override Task OnReconnected()
	//	{
	//		string text = base.Context.QueryString["username"];
	//		if (!_connections.GetConnections(text).Contains(base.Context.ConnectionId))
	//		{
	//			_connections.Add(text, base.Context.ConnectionId);
	//			obj.UserConnected(text, base.Context.ConnectionId);
	//		}
	//		return base.OnReconnected();
	//	}

	//	public void listofconnection()
	//	{
	//		base.Clients.All.list(_connections.GetConnections(""));
	//	}

	//	public void PrivateChat(string name, string UserId, string message)
	//	{
	//		base.Clients.Client(name).sendPrivateMessage(base.Context.ConnectionId, UserId, message);
	//		base.Clients.Caller.sendPrivateMessage(name, UserId, message);
	//	}

	//	public void SendClientMessage(string UserName, string message, string SendToConnectionId, string sendFromconnectionid)
	//	{
	//		base.Clients.User(SendToConnectionId).Messages(UserName, message, sendFromconnectionid);
	//	}

	//	public void Send(string name, string message)
	//	{
	//		IEnumerable<string> connections = _connections.GetConnections(name);
	//		foreach (string item in connections)
	//		{
	//			base.Clients.Client(item).ReceviedMessages(name, message);
	//		}
	//		MessageDetail messageDetail = new MessageDetail();
	//		messageDetail.RecevierName = name;
	//		messageDetail.SenderName = base.Context.QueryString["username"];
	//		messageDetail.Message = message;
	//		Task<bool> task = obj.SaveMessage(messageDetail);
	//		base.Clients.All.broadcastMessage(name, message);
	//	}

	//	public void SendNotification(string name, string message)
	//	{
	//		IEnumerable<string> connections = _connections.GetConnections(name);
	//		foreach (string item in connections)
	//		{
	//			base.Clients.Client(item).SendNotification(name, message);
	//		}
	//	}
	//}
}

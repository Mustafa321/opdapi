//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OPDAPI.Models
{
    using System;
    
    public partial class GetallMessages_Result
    {
        public Nullable<long> RNumber { get; set; }
        public decimal MessageId { get; set; }
        public string Message { get; set; }
        public Nullable<System.DateTime> SendingTime { get; set; }
        public Nullable<decimal> SenderId { get; set; }
        public Nullable<decimal> ReceiverId { get; set; }
        public Nullable<System.DateTime> ViewTime { get; set; }
        public Nullable<bool> IsRead { get; set; }
    }
}

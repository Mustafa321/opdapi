﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OPDAPI.Models
{
    public class PatientImages
    {
        public int PatientId { get; set; }

        public List<Imagess> Images { get; set; }
    }
    public class Imagess
    {
        public string Img { get; set; }

        public string ext { get; set; }
    }
}
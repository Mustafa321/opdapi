﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OPDAPI.Models
{
    public class Doctor
    {
        public decimal Id { get; set; }

        public decimal UserId { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Title { get; set; }

        public decimal RevisitCharges { get; set; }

        public string BloodGroup { get; set; }

        public string CNIC { get; set; }

        public string Designation { get; set; }

        public string MartialStatus { get; set; }

        public string DoctorName { get; set; }

        public string Email { get; set; }

        public string Charges { get; set; }

        public string WhatsappNumber { get; set; }

        public string Address { get; set; }

        public string DOB { get; set; }

        public string Gender { get; set; }

        public decimal ClinicId { get; set; }

        public int RowStart { get; set; }

        public int RowEnd { get; set; }
    }
}
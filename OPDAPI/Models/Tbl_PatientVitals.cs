//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OPDAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_PatientVitals
    {
        public decimal Id { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<decimal> PatientId { get; set; }
        public string PresentingComplaints { get; set; }
        public string Diagnosis { get; set; }
        public string Signs { get; set; }
        public string BP { get; set; }
        public string Pulse { get; set; }
        public string Temprature { get; set; }
        public string Weight { get; set; }
        public string Height { get; set; }
        public Nullable<bool> Active { get; set; }
        public string DisposalPlan { get; set; }
        public string Investigation { get; set; }
        public string HeadCircumferences { get; set; }
        public string Instruction { get; set; }
        public string History { get; set; }
        public string Status { get; set; }
        public string BMI { get; set; }
        public string BSA { get; set; }
        public Nullable<System.DateTime> FollowUpDate { get; set; }
        public Nullable<System.DateTime> VisitDate { get; set; }
        public string Notes { get; set; }
        public string HeartRate { get; set; }
        public string RespiratoryRate { get; set; }
        public Nullable<decimal> EnterBy { get; set; }
        public string EnterFrom { get; set; }
        public string PerscriptionFile { get; set; }
    
        public virtual Tbl_Patient Tbl_Patient { get; set; }
    }
}

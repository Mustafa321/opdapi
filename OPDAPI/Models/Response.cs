﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OPDAPI.Models
{
   
    public class Responce
    {
        public List<Object> data { get; set; }

        //  public Dictionary <string,object> myData { get; set; }

        // public List<object> itemsCount { get; set; }

        public ServiceResponseCode code { get; set; }
        public string message { get; set; }
        public string token { get; set; }
        public string Error { get; set; }
        public List<string> ErrorList { get; set; }
    }
    public enum ServiceResponseCode
    {
        OK = 200,
        UnAuthorized = 400,
        Error = 401,
        InternalError = 500,
        BadRequest = 403,
        NoResult = 402,
        Duplicate = 100,
        InvalidToken = 199,
        Unauthorized = 401,
        TooManyRequests = 429,
        MethodNotAllowed = 405,
        Created = 201 // Not applicable response in the result of creating an object. It should be combined a Location header location of the new resource

    }

}
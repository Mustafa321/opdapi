﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OPDAPI.Models
{
    public class ClinicsViewModel
    {
        public decimal ClinicId { get; set; }

        public string ClinicName { get; set; }
    }
}
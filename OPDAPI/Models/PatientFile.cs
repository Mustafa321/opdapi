﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OPDAPI.Models
{
    public class PatientFile
    {
        public string UserName { get; set; }

        public string FileName { get; set; }

        public string VisitDate { get; set; }
    }
}
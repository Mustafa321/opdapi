﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OPDAPI.Models
{
    public class MessageDetail
    {
        public decimal Id { get; set; }

        public string Date { get; set; }

        public bool isRead { get; set; }

        public string SenderName { get; set; }

        public string RecevierName { get; set; }

        public string Message { get; set; }

        public string UserName { get; set; }
    }
}
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OPDAPI.Models
{
    using System;
    
    public partial class GetDoctorByPatientId_Result
    {
        public Nullable<long> RNumber { get; set; }
        public decimal PatientId { get; set; }
        public Nullable<decimal> UserId { get; set; }
        public string PUserName { get; set; }
        public string DUserName { get; set; }
        public string Name { get; set; }
        public string MRNo { get; set; }
        public string Gender { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string WhatsappNumber { get; set; }
        public string MartialStatus { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public Nullable<System.DateTime> UpdationDate { get; set; }
        public string ClinicName { get; set; }
        public decimal ClinicId { get; set; }
        public decimal DoctorId { get; set; }
        public string DoctorName { get; set; }
        public string DoctorAddress { get; set; }
        public string DoctorCNIC { get; set; }
        public string DoctorDesignation { get; set; }
        public string DoctorEmail { get; set; }
        public string DoctorGender { get; set; }
        public string DoctorMartialStatus { get; set; }
        public string DoctorTitle { get; set; }
        public string DoctorWhatsAppNumber { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OPDAPI.Models
{
    public class Patient
    {
        public decimal Id { get; set; }
        public int? PatientId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string MRNo { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string WhatsappNumber { get; set; }
        public string Address { get; set; }
        public string cityName { get; set; }
        public string Email { get; set; }
        public string MartialStatus { get; set; }
        public string ClinicName { get; set; }
        public decimal? ClinicId { get; set; }
        public decimal? DoctorId { get; set; }
        public decimal? CityId { get; set; }
        public int UserId { get; set; }
        public string Age { get; set; }
        public string Years { get; set; }
        public string Months { get; set; }
        public string Days { get; set; }
        public bool isOnline { get; set; }
        public string RowStart { get; set; }
        public string RowEnd { get; set; }
        public string Message { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }

    }
    public class PatientMedicineMedicine
    {
        public string MedicineId { get; set; }
        public string OpdId { get; set; }
        public string MedicineName { get; set; }
        public string Dosage { get; set; }
        public string UrduDosage { get; set; }
        public string FarsiDosage { get; set; }
        public string Morning { get; set; }
        public string Night { get; set; }
        public string Evening { get; set; }
        public string Afternoon { get; set; }
        public string DaysCount { get; set; }
        public string Instruction { get; set; }


    }

    public class PatientVitals
    {
        public decimal? Id { get; set; }
        public decimal? DoctorId { get; set; }
        public int Patientid { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string opdid { get; set; }
        public string PresentingComplaints { get; set; }
        public string Diagnosis { get; set; }
        public string Signs { get; set; }
        public string BP { get; set; }
        public string Pulse { get; set; }
        public string Temprature { get; set; }
        public string Weight { get; set; }
        public string Height { get; set; }
        public string Active { get; set; }
        public string DisposalPlan { get; set; }
        public string Investigation { get; set; }
        public string HeadCircumferences { get; set; }
        public string Instruction { get; set; }
        public string History { get; set; }
        public string EnteredOn { get; set; }
        public string Status { get; set; }
        public string BMI { get; set; }
        public string BSA { get; set; }
        public string FollowUpDate { get; set; }
        public string VisitDate { get; set; }
        public string Notes { get; set; }
        public string HeartRate { get; set; }
        public string RespiratoryRate { get; set; }
        public string EnterBy { get; set; }
        public List<PatientMedicineMedicine> Medicinelist { get; set; }

    }

    public class PatientFiles
    {
        public decimal Id { get; set; }
        public decimal PatientId { get; set; }
        public string FileName { get; set; }
        public string Date { get; set; }
        public string UploadedDate { get; set; }
        public string FileType { get; set; }
        public string UploadedBy { get; set; }
        public string File { get; set; }
    }
}
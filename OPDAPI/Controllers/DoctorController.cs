﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OPDAPI.Models;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Web;
using System.IO;
using System.Web.Hosting;

namespace OPDAPI.Controllers
{
    [RoutePrefix("api/Doctor")]
    public class DoctorController : ApiController
    {
        OPDEntities db = new OPDEntities();


        [Route("DoctorRegistration")]
        [HttpPost]
        public async Task<bool> DoctorRegistration(Doctor Values)
        {
            bool res = false;

            try
            {
               
                if (Values.Id != 0)
                {
                    Tbl_User getuser = await db.Tbl_User.FirstOrDefaultAsync((x) => x.UserName == Values.UserName);
                    if (getuser != null)
                    {
                        Tbl_Doctors p2 = await db.Tbl_Doctors.FirstOrDefaultAsync(x => x.UserId == getuser.Id);
                        if (p2 != null)
                        {
                            p2.UserId = getuser.Id;
                            p2.Title = Values.Title;
                            p2.DoctorName = Values.DoctorName;
                            p2.Email = Values.Email;
                            p2.Designation = Values.Designation;
                            p2.WhatsappNumber = Values.WhatsappNumber;
                            p2.Address = Values.Address;
                            p2.DOB = Convert.ToDateTime(Values.DOB);
                            p2.BloodGroup = Values.BloodGroup;
                            p2.MartialStatus = Values.MartialStatus;
                            p2.Gender = Values.Gender;
                            db.Entry(p2).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                       
                            res = true;
                        }
                        

                    }
                    else
                    {
                        Tbl_Doctors p3 = new Tbl_Doctors();

                        p3.UserId = getuser.Id;
                        p3.Title = Values.Title;
                        p3.DoctorName = Values.DoctorName;
                        p3.Email = Values.Email;
                        p3.Designation = Values.Designation;
                        p3.WhatsappNumber = Values.WhatsappNumber;
                        p3.Address = Values.Address;
                        p3.DOB = Convert.ToDateTime(Values.DOB);
                        p3.BloodGroup = Values.BloodGroup;
                        p3.MartialStatus = Values.MartialStatus;
                        p3.Gender = Values.Gender;
                        db.Tbl_Doctors.Add(p3);
                        await db.SaveChangesAsync();
                        res = true;

                    }

                }

            }
            catch (Exception)
            {
                return false;
            }
            return res;
        }
        [Route("DoctorLogin")]
        [HttpPost]
        public async Task<Responce> Login(Doctor Values)
        {
            Responce i = new Responce();
            List<object> _obj = new List<object>();
            try
            {

                Tbl_User getuser = await db.Tbl_User.FirstOrDefaultAsync(x => x.UserName == Values.UserName && x.Password == Values.Password && x.IsDoctor != false);
                if (getuser != null)
                {
                    Tbl_Doctors get = await db.Tbl_Doctors.FirstOrDefaultAsync(x=> x.UserId ==getuser.Id && x.Active == true);
                    Doctor d = new Doctor
                    {
                        Id = get.Id,
                        UserId = getuser.Id,
                        UserName = getuser.UserName,
                        DoctorName = get.DoctorName,
                        Title = get.Title,
                        Email = get.Email,
                        DOB = Convert.ToDateTime(get.DOB).ToShortDateString(),
                        Gender = get.Gender,
                        WhatsappNumber = get.WhatsappNumber,
                        BloodGroup = get.BloodGroup,
                        Address = get.Address,
                        Designation = get.Designation
                    };
                    try
                    {
                        d.RevisitCharges = Convert.ToDecimal(get.RevisitCharges);
                    }
                    catch (Exception)
                    {
                    }
                    try
                    {
                        d.Charges = Convert.ToString(get.Charges);
                    }
                    catch (Exception)
                    {
                    }
                    d.MartialStatus = get.MartialStatus;
                    d.CNIC = get.CNIC;
                    _obj.Add(d);
                    i.data = _obj;
                    i.code = ServiceResponseCode.OK;
                }
                else
                {
                    i.code = ServiceResponseCode.NoResult;
                    i.message = "Invalid Login";
                }
                return i;
            }
            catch (Exception ex4)
            {
                Exception ex = ex4;
                i.data = null;
                i.code = ServiceResponseCode.Error;
                i.Error = ex.Message;
                return i;
            }
        }
        [Route("Get-Patient-By-Doctor-ClinicId")]
        [HttpPost]
        public async Task<Responce> GetPatientByDoctorClinicID(Doctor values)
        {
            Responce response = new Responce();
            List<object> _obj = new List<object>();
            try
            {
                List<GetPatientByDoctorAndClinicId_Result> list = db.GetPatientByDoctorAndClinicId(values.RowStart, values.RowEnd, values.Id, values.ClinicId).ToList();
                _obj.Add(list);
                response.data = _obj;
                response.code = ServiceResponseCode.OK;
                return response;
            }
            catch (Exception ex2)
            {
                Exception ex = ex2;
                response.code = ServiceResponseCode.Error;
                response.Error = ex.Message;
                response.data = null;
                return response;
            }
        }
        [Route("Get-Patient-By-Doctor-Id")]
        [HttpPost]
        public async Task<Responce> GetPatientByDoctorId(Doctor values)
        {
            Responce response = new Responce();
            List<object> _obj = new List<object>();
            try
            {
                if (values.RowStart <= 0)
                {
                    values.RowStart = 1;
                    values.RowEnd = 1000000;
                }
                List<GetPatientByDoctorId_Result> list = db.GetPatientByDoctorId(values.RowStart, values.RowEnd, values.Id).ToList();
                _obj.Add(list);
                response.data = _obj;
                response.code = ServiceResponseCode.OK;
                return response;
            }
            catch (Exception ex2)
            {
                Exception ex = ex2;
                response.code =ServiceResponseCode.Error;
                response.Error = ex.Message;
                response.data = null;
                return response;
            }
        }

        [Route("GetAllClinicByDoctorId")]
        [HttpPost]
        public async Task<Responce> GetallClinic(decimal Id)
        {
            Responce response = new Responce();
            List<object> _obj = new List<object>();
            try
            {
                IQueryable<IGrouping<decimal?, Tbl_DoctorAndPatientJoint>> GetClinicID = from x in db.Tbl_DoctorAndPatientJoint
                                                                                         where x.DoctorId == (decimal?)Id
                                                                                         group x by x.ClinicId;
                List<ClinicsViewModel> list = new List<ClinicsViewModel>();
                foreach (IGrouping<decimal?, Tbl_DoctorAndPatientJoint> item in GetClinicID)
                {
                    decimal? clinicId = item.FirstOrDefault().ClinicId;
                    ClinicsViewModel i = new ClinicsViewModel();
                    Tbl_Clinics getClinic = await db.Tbl_Clinics.FirstOrDefaultAsync(x =>x.Id == clinicId);
                    i.ClinicId = getClinic.Id;
                    i.ClinicName = getClinic.ClinicName;
                    list.Add(i);
                }
                _obj.Add(list);
                response.data = _obj;
                response.code = ServiceResponseCode.OK;
                return response;
            }
            catch (Exception ex2)
            {
                Exception ex = ex2;
                response.code = ServiceResponseCode.Error;
                response.Error = ex.Message;
                response.data = null;
                return response;
            }
        }
        [Route("SaveImages")]
        [HttpPost]
        public async Task<string> SaveImages(decimal PatientId)
        {
            try
            {
                if (await db.Tbl_Patient.FirstOrDefaultAsync(x => x.Id == PatientId) != null)
                {
                    HttpContext httpContext = HttpContext.Current;
                    if (httpContext.Request.Files.Count > 0)
                    {
                        for (int i = 0; i < httpContext.Request.Files.Count; i++)
                        {
                            Random rand = new Random();
                            string Datetime = Convert.ToDateTime(DateTime.Now).ToString("ddMMMyyyy");
                            int r = rand.Next(1, 9999);
                            HttpPostedFile httpPostedFile = httpContext.Request.Files[i];
                            string getFileName = r + "_" + Datetime + "_" + httpPostedFile.FileName;
                            string path = HttpContext.Current.Server.MapPath("~/Images/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            string fileSavePath = Path.Combine(HostingEnvironment.MapPath("~/Images/"), getFileName);
                            httpPostedFile.SaveAs(fileSavePath);
                            Tbl_PatientFiles j = new Tbl_PatientFiles
                            {
                                PatientId = PatientId,
                                Date = DateTime.Now,
                                FilesName = getFileName
                            };
                            db.Tbl_PatientFiles.Add(j);
                            await db.SaveChangesAsync();
                        }
                    }
                    return "true";
                }
                return "Patient Not Found";
            }
            catch (Exception ex2)
            {
                Exception ex = ex2;
                return ex.Message;
            }
        }
        [Route("SaveBaseImages")]
        [HttpPost]
        public async Task<string> SaveBaseImages(PatientImages obj)
        {
            string res = "";
            try
            {
                if (await db.Tbl_Patient.FirstOrDefaultAsync(x => x.Id ==obj.PatientId) != null)
                {
                    foreach (var item in obj.Images)
                    {
                        try
                        {
                            string path = HttpContext.Current.Server.MapPath("~/Images/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            DateTime nowToTheSecond = DateTime.Now;
                            TimeSpan span = nowToTheSecond - new DateTime(1970, 1, 1, 0, 0, 0, 0);
                            string filePath = HttpContext.Current.Server.MapPath("~/Images/" + span.Ticks + "." + item.ext);
                            item.Img = item.Img.Substring(item.Img.IndexOf(',') + 1);
                            File.WriteAllBytes(filePath, Convert.FromBase64String(item.Img));
                            string Image = span.Ticks + "." + item.ext;
                            Tbl_PatientFiles i = new Tbl_PatientFiles
                            {
                                PatientId = obj.PatientId,
                                Date = DateTime.Now,
                                FilesName = Image,
                                FileType = item.ext
                            };
                            db.Tbl_PatientFiles.Add(i);
                            await db.SaveChangesAsync();
                            res = "true";
                        }
                        catch (Exception ex2)
                        {
                            res = ex2.Message;
                        }
                    }
                }
                else
                {
                    res = "Patient Not Found";
                }
            }
            catch (Exception ex3)
            {
                Exception ex = ex3;
                return ex.Message;
            }
            return res;
        }

        [Route("GetPatientDetail")]
        [HttpGet]
        public async Task<Responce> GetPatientDetail(decimal PatientId)
        {
            Responce response = new Responce();
            List<object> _obj = new List<object>();
            try
            {
                List<GetPatientDetail_Result> list = db.GetPatientDetail(PatientId).ToList();
                _obj.Add(list);
                response.data = _obj;
                response.code = ServiceResponseCode.OK;
                return response;
            }
            catch (Exception ex2)
            {
                Exception ex = ex2;
                response.code = ServiceResponseCode.Error;
                response.Error = ex.Message;
                response.data = null;
                return response;
            }
        }
        [HttpGet]
        [Route("GetPatientDetailVitalsOnly")]
        public Responce GetPatientDetailVitalsOnly(int PatientId)
        {
            Responce res = new Responce();
            try
            {

                var getdetail = db.GetPatientDetailVitalsOnly(PatientId);
                List<object> list = new List<object>();
                list.Add(getdetail);
                res.code = ServiceResponseCode.OK;
                res.data = list;
            }
            catch (Exception ex)
            {
                res.code = ServiceResponseCode.Error;
                res.Error = ex.Message;
            }
            return res;
        }
    }
}

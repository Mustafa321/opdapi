﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OPDAPI.Models;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Web;
using System.IO;

namespace OPDAPI.Controllers
{
    [RoutePrefix("api/Patient")]

    public class PatientController : ApiController
    {
        OPDEntities db = new OPDEntities();

        [HttpPost]
        [Route("PatientLogin")]
        public Responce PatientLogin(Patient n)
        {
            Responce res = new Responce();
            try
            {
                var User = db.Tbl_User.FirstOrDefault(x => x.UserName == n.UserName && x.Password == n.Password);
                if (User == null)
                {
                    res.code = ServiceResponseCode.NoResult;
                    res.message = "User Not Found";

                }
                else
                {
                    var getp = db.Tbl_Patient.FirstOrDefault(x => x.UserId == User.Id);
                    Patient p = new Patient();
                    p.Id = Convert.ToInt32(getp.Id);
                    p.UserId = Convert.ToInt32(getp.UserId);
                    p.UserName = User.UserName;
                    p.MRNo = getp.MRNo;
                    p.Name = getp.Name;
                    p.Gender = getp.Gender;
                    try
                    {
                        p.DOB = Convert.ToDateTime(getp.DOB).ToString("dd/MM/yyyy");
                        var age = DateTime.Now.Year - Convert.ToDateTime(getp.DOB).Year;
                        p.Age = age.ToString();

                    }
                    catch (Exception ex)
                    {

                    }
                    p.WhatsappNumber = getp.WhatsappNumber;
                    p.Address = getp.Address;
                    try
                    {
                        p.DoctorId = (decimal)getp.Tbl_DoctorAndPatientJoint.FirstOrDefault(x => x.PatientID == getp.Id).DoctorId;

                    }
                    catch (Exception ex)
                    {

                    }
                    List<object> list = new List<object>();
                    list.Add(p);
                    res.data = list;
                    res.code = ServiceResponseCode.OK;
                }
            }
            catch(Exception ex)
            {
                res.code = ServiceResponseCode.Error;
                res.Error = ex.Message;
                
            }
            return res;
        }
  
        [Route("PatientRegister")]
        [HttpPost]
        public async Task<bool> PatientRegister(List<Patient> OBj)
        {
            try
            {
                bool res=false;
                foreach (var Values in OBj)
                {
                    if (Values.Id == 0)
                    {
                        Tbl_User getuser = await db.Tbl_User.FirstOrDefaultAsync((x) => x.UserName == Values.UserName);
                        if (getuser != null)
                        {
                            Tbl_Patient p2 = await db.Tbl_Patient.FirstOrDefaultAsync(x => x.UserId == getuser.Id);
                            p2.UserId = getuser.Id;
                            p2.MRNo = Values.MRNo;
                            p2.Name = Values.Name;
                            p2.MartialStatus = Values.MartialStatus;
                            p2.Email = Values.Email;
                            p2.Gender = Values.Gender;
                            p2.DOB = Convert.ToDateTime(Values.DOB);
                            p2.WhatsappNumber = Values.WhatsappNumber;
                            p2.Address = Values.Address;
                            p2.UpdationDate = DateTime.Now;
                            res = true;
                        }
                        else
                        {
                            Tbl_User u2 = new Tbl_User
                            {
                                UserName = Values.UserName,
                                Password = Values.Password,
                                IsDoctor = false
                            };
                            db.Tbl_User.Add(u2);
                            await db.SaveChangesAsync();
                            Tbl_Patient s2 = new Tbl_Patient
                            {
                                UserId = u2.Id,
                                MRNo = Values.MRNo,
                                MartialStatus = Values.MartialStatus,
                                Email = Values.Email,
                                Name = Values.Name,
                                Gender = Values.Gender,
                                RegistrationDate = DateTime.Now,
                                UpdationDate = DateTime.Now,
                                DOB = Convert.ToDateTime(Values.DOB),
                                WhatsappNumber = Values.WhatsappNumber,
                                Address = Values.Address
                            };
                            db.Tbl_Patient.Add(s2);
                            await db.SaveChangesAsync();
                            Tbl_DoctorAndPatientJoint d2 = new Tbl_DoctorAndPatientJoint
                            {
                                PatientID = s2.Id,
                                DoctorId = Values.DoctorId,
                                ClinicId = Convert.ToDecimal(Values.ClinicId)
                            };
                            db.Tbl_DoctorAndPatientJoint.Add(d2);
                            await db.SaveChangesAsync();
                            res = true;
                        }
                    }
                    else
                    {
                        Tbl_User getuser = await db.Tbl_User.FirstOrDefaultAsync(x => x.UserName == Values.UserName);
                        if (getuser != null)
                        {
                            Tbl_Patient p = await db.Tbl_Patient.FirstOrDefaultAsync(x => x.UserId == getuser.Id);
                            p.UserId = getuser.Id;
                            p.MRNo = Values.MRNo;
                            p.Name = Values.Name;
                            p.MartialStatus = Values.MartialStatus;
                            p.Email = Values.Email;
                            p.Gender = Values.Gender;
                            p.DOB = Convert.ToDateTime(Values.DOB);
                            p.WhatsappNumber = Values.WhatsappNumber;
                            p.Address = Values.Address;
                            p.UpdationDate = DateTime.Now;
                            db.Entry(p).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                        }
                        else
                        {
                            Tbl_User u = new Tbl_User
                            {
                                Password = Values.Password,
                                IsDoctor = false
                            };
                            db.Entry(u).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                            Tbl_Patient s = new Tbl_Patient
                            {
                                UserId = u.Id,
                                MRNo = Values.MRNo,
                                MartialStatus = Values.MartialStatus,
                                Email = Values.Email,
                                Name = Values.Name,
                                Gender = Values.Gender,
                                RegistrationDate = DateTime.Now,
                                UpdationDate = DateTime.Now,
                                DOB = Convert.ToDateTime(Values.DOB),
                                WhatsappNumber = Values.WhatsappNumber,
                                Address = Values.Address
                            };
                            db.Tbl_Patient.Add(s);
                            await db.SaveChangesAsync();
                            Tbl_DoctorAndPatientJoint d = new Tbl_DoctorAndPatientJoint
                            {
                                PatientID = s.Id,
                                DoctorId = Values.DoctorId,
                                ClinicId = Convert.ToDecimal(Values.ClinicId)
                            };
                            db.Tbl_DoctorAndPatientJoint.Add(d);
                            await db.SaveChangesAsync();
                        }
                        res = true;
                    }
                }
              
                return res;
            }
            catch (Exception)
            {
                return false;
            }
        }
        [Route("ChangePassword")]
        [HttpPost]
        public async Task<bool> ChangePassword(Patient OBj)
        {
            try
            {
                bool res = false;

                var getpatient = await db.Tbl_User.FirstOrDefaultAsync(x => x.UserName == OBj.UserName);
                if(getpatient!=null)
                {
                    getpatient.Password = OBj.Password;
                    db.Entry(getpatient).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    res = true;
                }
                else
                {
                    res = false;
                }

                return res;
            }
            catch (Exception)
            {
                return false;
            }
        }
        [Route("GetPatientMedicine")]
        [HttpPost]
        public  List<GetPatientMedicine_Result> GetPatientMedicine(int PatientId)
        {
            try
            {

                var getpatient = db.GetPatientMedicine(PatientId).ToList();
                return getpatient;
            }
            catch (Exception)
            {
                return null;
            }
        }
        [Route("CheckInternetConnection")]
        [HttpGet]
        public string CheckInternetConnection(Patient Values)
        {
            return "true";
        }
        
        [HttpPost]
        [Route("SavePatientOpdDetail")]
        public async Task< string> SavePatientOpdDetail(PatientVitals n)
        {
            string res = "";
            try
            {
                var getP = db.Tbl_Patient.FirstOrDefault(x => x.Id == n.Patientid);
                if(getP!=null)
                {

                    var d = new Tbl_PatientVitals();
                    d.PatientId = getP.Id;
                    d.PresentingComplaints = n.PresentingComplaints;
                    d.Diagnosis = n.Diagnosis;
                    d.Signs = n.Signs;
                    d.BP = n.BP;
                    d.Pulse = n.Pulse;
                    d.Temprature = n.Temprature;
                    d.Weight = n.Weight;
                    d.Height = n.Height;
                    d.DisposalPlan = n.DisposalPlan;
                    d.Investigation = n.Investigation;
                    d.HeadCircumferences = n.HeadCircumferences;
                    d.Instruction = n.Instruction;
                    d.History = n.History;
                    d.Status = n.Status;
                    d.BMI = n.BMI;
                    d.BSA = n.BSA;
                    try
                    {
                        d.VisitDate = Convert.ToDateTime(n.VisitDate);
                    }
                    catch (Exception ex)
                    {

                    }
                    try
                    {
                        d.FollowUpDate = Convert.ToDateTime(n.FollowUpDate);
                    }
                    catch (Exception ex)
                    {

                    }
                    d.Notes = n.Notes;
                    d.HeartRate = n.HeartRate;
                    d.RespiratoryRate = n.RespiratoryRate;
                    try
                    {

                        d.EnterBy = Convert.ToDecimal(n.EnterBy);
                    }
                    catch (Exception ex)
                    {
                    }
                    db.Tbl_PatientVitals.Add(d);
                    await db.SaveChangesAsync();
                    res = "true";
                }
                else
                {
                    res = "Patient Not Found";
                }
            }
            catch(Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }

        [HttpPost]
        [Route("SavePatientDetailOPD")]
        public async Task<string> SavePatientDetailOPD(List<PatientVitals> Obj)
        {
            string res = "";
            try
            {
                foreach(var n in Obj)
                {
                    var getUser = await db.Tbl_User.FirstOrDefaultAsync(x => x.UserName == n.UserName);
                    if(getUser!=null)
                    {
                        var getP = await db.Tbl_Patient.FirstOrDefaultAsync(x => x.UserId == getUser.Id);
                        if (getP != null)
                        {
                            var d = new Tbl_PatientVitals();
                            d.PatientId = getP.Id;
                            d.PresentingComplaints = n.PresentingComplaints;
                            d.Diagnosis = n.Diagnosis;
                            d.Signs = n.Signs;
                            d.BP = n.BP;
                            d.Pulse = n.Pulse;
                            d.Temprature = n.Temprature;
                            d.Weight = n.Weight;
                            d.Height = n.Height;
                            d.DisposalPlan = n.DisposalPlan;
                            d.Investigation = n.Investigation;
                            d.HeadCircumferences = n.HeadCircumferences;
                            d.Instruction = n.Instruction;
                            d.History = n.History;
                            d.Status = n.Status;
                            d.BMI = n.BMI;
                            d.BSA = n.BSA;
                            d.Date = DateTime.Now;
                            try
                            {
                                d.VisitDate = Convert.ToDateTime(n.VisitDate);
                            }
                            catch (Exception ex)
                            {

                            }
                            try
                            {
                                d.FollowUpDate = Convert.ToDateTime(n.FollowUpDate);
                            }
                            catch (Exception ex)
                            {

                            }
                            d.Notes = n.Notes;
                            d.HeartRate = n.HeartRate;
                            d.RespiratoryRate = n.RespiratoryRate;
                            try
                            {

                                d.EnterBy = Convert.ToDecimal(n.EnterBy);
                            }
                            catch (Exception ex)
                            {
                            }
                            db.Tbl_PatientVitals.Add(d);
                            await db.SaveChangesAsync();

                          
                            foreach(var item in n.Medicinelist)
                            {
                                var s = new Tbl_PMedicine();
                                s.OpdId =Convert.ToInt32( d.Id);
                                s.MedicineName = item.MedicineName;
                                s.PatientId = n.Patientid;
                                s.Dosage = item.Dosage;
                                db.Tbl_PMedicine.Add(s);
                                await db.SaveChangesAsync();
                            }

                            res = "true";
                        }
                        else
                        {
                            res = "Patient Not Found";
                        }
                    }
                    else
                    {

                    }
                   
                }

            
            }
            catch (Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }

        [HttpGet]
        [Route("Get-Document-By-PatientId")]
        public Responce GetDocumentByPatientId(int PatientId)
        {
            var n = new Responce();

            try
            {
                var get = db.GetPatientDocuments(PatientId);

                List<object> list = new List<object>();
                list.Add(get);
                n.data = list;
                n.code = ServiceResponseCode.OK;

            }
            catch (Exception ex)
            {
                n.code = ServiceResponseCode.Error;
                n.Error = ex.Message;
            }
            return n;
        }
        [HttpPost]
        [Route("Get-Patient-By-Search")]
        public Responce GetPAtientBySearch(Patient n)
        {
            Responce res = new Responce();

            try
            {
                if (n.RowStart == null)
                {
                    var get = db.SearchPatient(1,10000000, n.Name).ToList() ;
                    List<object> list = new List<object>();
                    list.Add(get);
                    res.data = list;
                    res.code = ServiceResponseCode.OK;
                }
                else if (n.RowStart != null && n.RowEnd != null)
                {
                    var RowStart = Convert.ToInt32(n.RowStart);
                    var RowEnd = Convert.ToInt32(n.RowEnd);
                    var get = db.SearchPatient(RowStart, RowEnd, n.Name).ToList();

                    List<object> list = new List<object>();
                    list.Add(get);
                    res.data = list;
                    res.code = ServiceResponseCode.OK;
                }
            }
            catch(Exception ex)
            {
                res.code = ServiceResponseCode.Error;
                res.message = ex.Message;
            }
           
            return res;

        }

        [Route("Get-Doctor-By-Patient-ClinicId")]
        [HttpPost]
        public async Task<Responce> GetDoctorByPatientAndClinicId(Doctor values)
        {
            Responce response = new Responce();
            List<object> _obj = new List<object>();
            try
            {
                List<GetDoctorByPatientAndClinicId_Result> list = db.GetDoctorByPatientAndClinicId(values.Id, values.ClinicId).ToList();
                _obj.Add(list);
                response.data = _obj;
                response.code = ServiceResponseCode.OK;
                return response;
            }
            catch (Exception ex2)
            {
                Exception ex = ex2;
                response.code = ServiceResponseCode.Error;
                response.Error = ex.Message;
                response.data = null;
                return response;
            }
        }
        [Route("Get-Doctor-By-PatientId")]
        [HttpGet]
        public async Task<Responce> GetDoctorByPatientId(decimal PatientId)
        {
            Responce response = new Responce();
            List<object> _obj = new List<object>();
            try
            {
                List<GetDoctorByPatientId_Result> list = db.GetDoctorByPatientId(PatientId).ToList();
                _obj.Add(list);
                response.data = _obj;
                response.code = ServiceResponseCode.OK;
                return response;
            }
            catch (Exception ex2)
            {
                Exception ex = ex2;
                response.code = ServiceResponseCode.Error;
                response.Error = ex.Message;
                response.data = null;
                return response;
            }
        }

        [Route("GetAllClinicByPatientId")]
        [HttpPost]
        public async Task<Responce> GetAllClinicByPatientId(decimal Id)
        {
            Responce response = new Responce();
            List<object> _obj = new List<object>();
            try
            {
                IQueryable<IGrouping<decimal?, Tbl_DoctorAndPatientJoint>> GetClinicID = from x in db.Tbl_DoctorAndPatientJoint
                                                                                         where x.PatientID == (decimal?)Id
                                                                                         group x by x.ClinicId;
                List<ClinicsViewModel> list = new List<ClinicsViewModel>();
                foreach (IGrouping<decimal?, Tbl_DoctorAndPatientJoint> item in GetClinicID)
                {
                    decimal? clinicId = item.FirstOrDefault().ClinicId;
                    ClinicsViewModel i = new ClinicsViewModel();
                    Tbl_Clinics getClinic = await db.Tbl_Clinics.FirstOrDefaultAsync((Tbl_Clinics x) => (decimal?)x.Id == clinicId);
                    i.ClinicId = getClinic.Id;
                    i.ClinicName = getClinic.ClinicName;
                    list.Add(i);
                }
                _obj.Add(list);
                response.data = _obj;
                response.code = ServiceResponseCode.OK;
                return response;
            }
            catch (Exception ex2)
            {
                Exception ex = ex2;
                response.code = ServiceResponseCode.Error;
                response.Error = ex.Message;
                response.data = null;
                return response;
            }
        }

        [HttpPost]
        [Route("SaveOPDDoc")]
        public async Task<string> SaveOPDDoc(PatientFile n)
        {

            string res = "";
            Tbl_User getuser = await db.Tbl_User.FirstOrDefaultAsync(x => x.UserName == n.UserName);
            var getpatient = db.Tbl_Patient.FirstOrDefault(x => x.UserId == getuser.Id);
            var VDate = Convert.ToDateTime(n.VisitDate);
            Tbl_PatientPerscription getvital = db.Tbl_PatientPerscription.FirstOrDefault(x => x.Date == VDate && x.PatientId == getpatient.Id);
            if (getpatient != null && getvital == null)
            {
                string Image;
                try
                {
                    DateTime nowToTheSecond = DateTime.Now;
                    TimeSpan span = nowToTheSecond - new DateTime(1970, 1, 1, 0, 0, 0, 0);
                    string filePath = HttpContext.Current.Server.MapPath("~/PrescriptionDocs/" + span.Ticks + ".pdf");
                    n.FileName = n.FileName.Substring(n.FileName.IndexOf(',') + 1);
                    File.WriteAllBytes(filePath, Convert.FromBase64String(n.FileName));
                    Image = span.Ticks + ".pdf";
                }
                catch (Exception)
                {
                    Image = "";
                }
                if (Image != "")
                {
                    try
                    {
                        if (getpatient != null)
                        {
                            Tbl_PatientPerscription file = new Tbl_PatientPerscription
                            {
                                DocumentNam = "/PrescriptionDocs/" + Image,
                                Date = VDate
                            };
                            db.Tbl_PatientPerscription.Add(file);
                            await db.SaveChangesAsync();
                        }
                        res = "true";
                    }
                    catch (Exception)
                    {
                        res = "false";
                    }
                }
            }
            else
            {
                res = "false";
            }
            return res;
        }

        [Route("GetPatientPrescriptionDoc")]
        [HttpGet]
        public async Task<Responce> GetPatientPrescriptionDoc(int PatientId)
        {
            Responce response = new Responce();
            List<object> _obj = new List<object>();
            try
            {
                List<GetPerscriptionDocument_Result> list = db.GetPerscriptionDocument(PatientId).ToList();
                _obj.Add(list);
                response.data = _obj;
                response.code = ServiceResponseCode.OK;
                return response;
            }
            catch (Exception ex2)
            {
                Exception ex = ex2;
                response.code = ServiceResponseCode.Error;
                response.Error = ex.Message;
                response.data = null;
                return response;
            }
        }

    }
}

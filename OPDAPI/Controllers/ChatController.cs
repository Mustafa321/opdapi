﻿using OPDAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace OPDAPI.Controllers
{
    [RoutePrefix("api/Chat")]
    public class ChatController : ApiController
    {
        OPDEntities db = new OPDEntities();
        public async Task<bool> UserConnected(string UserName, string ConnectionId)
        {
            Tbl_User getUser = await db.Tbl_User.FirstOrDefaultAsync(x => x.UserName == UserName);
            if (getUser != null)
            {
                Tbl_MessageConnection i = new Tbl_MessageConnection
                {
                    UserId = getUser.Id,
                    ConnectionId = ConnectionId,
                    IsDoctor = false
                };
                db.Tbl_MessageConnection.Add(i);
                await db.SaveChangesAsync();
            }
            return true;
        }
        public async Task<bool> UserDisconnected(string ConnectionId)
        {
            Tbl_MessageConnection get = await db.Tbl_MessageConnection.FirstOrDefaultAsync(x => x.ConnectionId == ConnectionId);
            if (get != null)
            {
                db.Tbl_MessageConnection.Remove(get);
                await db.SaveChangesAsync();
            }
            return true;
        }
        [Route("Get-Doctor-ContactList")]
        [HttpPost]
        public async Task<Responce> GetDoctorContactList(Doctor values)
        {
            Responce response = new Responce();
            List<object> _obj = new List<object>();
            try
            {
                List<GetPatientByDoctorAndClinicId_Result> list = db.GetPatientByDoctorAndClinicId(values.RowStart, values.RowEnd, values.Id, values.ClinicId).ToList();
                List<Patient> p = new List<Patient>();
                foreach (GetPatientByDoctorAndClinicId_Result item in list)
                {
                    Tbl_MessageConnection getOnlineStatus = await db.Tbl_MessageConnection.FirstOrDefaultAsync(x => x.IsDoctor == false && x.UserId == item.PatientId);
                    Patient y = new Patient
                    {
                        UserName = item.PUserName,
                        Id =Convert.ToInt32( item.PatientId),
                        Name = item.Name
                    };
                    if (getOnlineStatus != null)
                    {
                        y.isOnline = true;
                    }
                    else
                    {
                        y.isOnline = false;
                    }
                    p.Add(y);
                }
                _obj.Add(p);
                response.data = _obj;
                response.code = ServiceResponseCode.OK;
                return response;
            }
            catch (Exception ex2)
            {
                Exception ex = ex2;
                response.code = ServiceResponseCode.Error;
                response.Error = ex.Message;
                response.data = null;
                return response;
            }
        }
        [Route("Save-Messages")]
        [HttpPost]
        public async Task<bool> SaveMessage(MessageDetail obj)
        {
            try
            {
                Message i = new Message
                {
                    DeleteByReceiver = false,
                    DeleteBySender = false,
                    IsRead = false,
                    Message1 = obj.Message,
                    SendingTime = DateTime.Now
                };
                Tbl_User getSender = db.Tbl_User.FirstOrDefault(x=> x.UserName == obj.SenderName);
                Tbl_User getReceiver = db.Tbl_User.FirstOrDefault( x=> x.UserName == obj.RecevierName);
                i.ReceiverId = getReceiver.Id;
                i.SenderId = getSender.Id;
                if (getSender.IsDoctor == false)
                {
                    i.SendByDoctor = false;
                }
                else
                {
                    i.SendByDoctor = true;
                }
                if (getReceiver.IsDoctor == false)
                {
                    i.SendByDoctor = false;
                }
                else
                {
                    i.SendByDoctor = true;
                }
                db.Messages.Add(i);
                await db.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        [Route("Get-Conversation")]
        [HttpPost]
        public async Task<List<GetallMessages_Result>> GetConversation(decimal senderId, decimal ReceiverId)
        {
            try
            {
                return db.GetallMessages(senderId, ReceiverId, 10000, 1).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
        [Route("GetUnReadMessagesDetail")]
        [HttpGet]
        public async Task<List<Patient>> GetUnReadMessagesDetail(decimal DoctorId)
        {
            try
            {
                List<decimal?> getall = db.GetNotificationforDoctor(DoctorId).ToList();
                List<Patient> list = new List<Patient>();
                foreach (decimal? item in getall)
                {
                    decimal PatientId = item.Value;
                    Tbl_Patient p = await db.Tbl_Patient.FirstOrDefaultAsync(x => x.UserId == PatientId);
                    Message getmessg = await (from x in db.Messages
                                              where x.SenderId == (decimal?)PatientId
                                              orderby x.MessageId descending
                                              select x).FirstOrDefaultAsync();
                    list.Add(new Patient
                    {
                        MRNo = p.MRNo,
                        Name = p.Name,
                        Message = getmessg.Message1,
                        Date = Convert.ToDateTime(getmessg.SendingTime).ToString("dd MMM yyyy"),
                        Time = Convert.ToDateTime(getmessg.SendingTime).ToShortTimeString()
                    });
                }
                return list;
            }
            catch (Exception)
            {
                return null;
            }
        }
        [Route("GetUnReadMessagesCount")]
        [HttpGet]
        public async Task<int> GetUnReadMessagesCount(decimal DoctorId)
        {
            try
            {
                List<decimal?> getall = db.GetNotificationforDoctor(DoctorId).ToList();
                return getall.Count();
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}

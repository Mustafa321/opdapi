﻿using OPDAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace OPDAPI.Controllers
{
    public class ClinicsController : ApiController
    {
        OPDEntities db = new OPDEntities();

        [Route("AddClinic")]
        [HttpPost]
        public async Task<IHttpActionResult> AddClinic(Tbl_Clinics Values)
        {
            try
            {
                string res;
                if (Values.Id > 0m)
                {
                    db.Entry(Values).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    res = "Clinic Updated Successfully";
                }
                else
                {
                    db.Tbl_Clinics.Add(Values);
                    await db.SaveChangesAsync();
                    res = "Add Clinic Successfully";
                }
                return Json(new
                {
                    message = res,
                    StatusCode = true
                });
            }
            catch (Exception ex2)
            {
                Exception ex = ex2;
                return Json(new
                {
                    message = ex.Message,
                    StatusCode = false
                });
            }
        }
    }
}
